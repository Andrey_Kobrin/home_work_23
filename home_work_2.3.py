import glob
import os.path

migrations = 'Migrations'

def files_search():
	files = glob.glob(os.path.join(migrations, "*.sql"))
	while len(files) > 0:
		print('Введите слово' )
		word = input ()
		files = word_search(files, word)
		print('\n'.join(map(str, files)))
		print(len(files))

def word_search(files, word):
	files_founded = []
	for file in files:
		f = open(file,'r')
		line = f.readline()
		if word in line:
			files_founded.append(file)
	return(files_founded)

files_search()